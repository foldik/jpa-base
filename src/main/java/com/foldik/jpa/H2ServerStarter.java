package com.foldik.jpa;

import org.h2.tools.Server;

import java.sql.SQLException;

public class H2ServerStarter {

    public static void main(String[] args) throws SQLException {
        startServer();
    }

    public static void startServer() throws SQLException {
        Server.createWebServer("-webPort", "8997").start();
    }
}
