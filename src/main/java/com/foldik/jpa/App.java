package com.foldik.jpa;

import com.foldik.jpa.repository.EmployeeRepository;
import com.foldik.jpa.repository.Example;
import com.foldik.jpa.repository.PhoneRepository;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.SQLException;

public class App {

    public static void main(String[] args) throws SQLException {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("my_persistence_unit");
        new Example(
                new EmployeeRepository(entityManagerFactory.createEntityManager()),
                new PhoneRepository(entityManagerFactory.createEntityManager())
        ).run();
        H2ServerStarter.startServer();
    }
}
