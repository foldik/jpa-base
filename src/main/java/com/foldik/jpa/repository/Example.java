package com.foldik.jpa.repository;

import com.foldik.jpa.repository.model.Employee;
import com.foldik.jpa.repository.model.Phone;
import org.apache.log4j.Logger;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class Example {

    private static final Logger LOGGER = Logger.getLogger(Example.class);

    private EmployeeRepository employeeRepository;
    private PhoneRepository phoneRepository;

    public Example(EmployeeRepository employeeRepository, PhoneRepository phoneRepository) {
        this.employeeRepository = employeeRepository;
        this.phoneRepository = phoneRepository;
    }

    public void run() {
        insertRandomEmployeeEveryFiveSeconds();
        insertRandomPhoneEveryFiveSeconds();
    }

    private void insertRandomEmployeeEveryFiveSeconds() {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Employee employee = new Employee();
                employee.setName(UUID.randomUUID().toString());
                employeeRepository.save(employee);
                LOGGER.info("Saved: " + employee);
            }
        }, 10, 5000);
    }

    private void insertRandomPhoneEveryFiveSeconds() {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Phone phone = new Phone();
                phone.setType("Samsung Galaxy S3");
                phone.setNumber(new Random().nextInt(1000000));
                phoneRepository.save(phone);
                LOGGER.info("Saved: " + phone);
            }
        }, 10, 5000);
    }
}
