package com.foldik.jpa.repository;

import com.foldik.jpa.repository.model.Employee;

import javax.persistence.EntityManager;

public class EmployeeRepository {

    private EntityManager entityManager;

    public EmployeeRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Employee save(Employee employee) {
        entityManager.getTransaction().begin();
        entityManager.persist(employee);
        System.out.println("Saved:" + employee);
        entityManager.getTransaction().commit();
        return employee;
    }
}





