package com.foldik.jpa.repository;

import com.foldik.jpa.repository.model.Phone;

import javax.persistence.EntityManager;

public class PhoneRepository {

    private EntityManager entityManager;

    public PhoneRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Phone save(Phone phone) {
        entityManager.getTransaction().begin();
        entityManager.persist(phone);
        System.out.println("Saved:" + phone);
        entityManager.getTransaction().commit();
        return phone;
    }

}
