package com.foldik.jpa.repository.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "track")
public class Track {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "training_id")
    private long trainingId;

    @Column(name = "speed")
    private BigDecimal speed;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TRAINING_ID")
    private Training training;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(long trainingId) {
        this.trainingId = trainingId;
    }

    public BigDecimal getSpeed() {
        return speed;
    }

    public void setSpeed(BigDecimal speed) {
        this.speed = speed;
    }
}
