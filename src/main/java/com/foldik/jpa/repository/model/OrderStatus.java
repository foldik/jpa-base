package com.foldik.jpa.repository.model;

public enum OrderStatus {

    NEW,
    PENDING,
    DELIVERED,
    CLOSED
}
