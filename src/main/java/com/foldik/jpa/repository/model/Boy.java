package com.foldik.jpa.repository.model;

import javax.persistence.*;

@Entity
@Table(name = "boy")
public class Boy {

    @Id
    @GeneratedValue
    @Column(name = "BOY_ID")
    private Long id;

    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "GIRL_ID")
    private Girl girl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Girl getGirl() {
        return girl;
    }

    public void setGirl(Girl girl) {
        this.girl = girl;
    }
}
