package com.foldik.jpa.repository.model;

import javax.persistence.*;

@Entity
@Table(name = "girl")
public class Girl {

    @Id
    @GeneratedValue
    @Column(name = "GIRL_ID")
    private Long id;

    private String name;

    @OneToOne(mappedBy = "girl")
    private Boy boy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boy getBoy() {
        return boy;
    }

    public void setBoy(Boy boy) {
        this.boy = boy;
    }
}
